#!/bin/sh

TARGET=""

while getopts "aim" option
do
    case "$option"
    in
    a) gomobile bind -v -target android -trimpath -ldflags="-s -w" gitlab.com/mb-saces/pinecone/build/gobind ;;
    i) gomobile bind -v -target ios -trimpath -ldflags="" gitlab.com/mb-saces/pinecone/build/gobind ;;
    m) gomobile bind -v -target macos -trimpath -ldflags="" gitlab.com/mb-saces/pinecone/build/gobind ;;
    *) echo "No target specified, specify -a or -i"; exit 1 ;;
    esac
done